#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class graph{
   private:
   int no_vertex;
   vector<int> *list_graph;

   public:
   graph(){
      list_graph=NULL;
   }
   graph(int vertex){
      this->no_vertex=vertex;
      this->list_graph = new vector<int>[vertex];
   }

   void addToGraph(int adder,int new_friend){
      this->list_graph[adder].push_back(new_friend);
   }

   void printGraph(){
      for (int v = 0; v < this->no_vertex; ++v){
         cout << "Friend list of "<< v << " --> [ ";
         for(int i=0;i<list_graph[v].size();i++){
            cout << list_graph[v][i] << " ";
         }
         cout<<"]"<<endl;
      }
   }

   void BFS(int start_from){
      bool *visited=new bool[this->no_vertex];
      for(int i=0;i<this->no_vertex; i++){
         visited[i]=false;
      }
      vector<int> queue;
      visited[start_from] = true;
      queue.push_back(start_from);

      while(!queue.empty()){
         start_from = queue.front();
         cout << start_from << " ";
         queue.erase(queue.begin());
         for (int i = 0; i<list_graph[start_from].size(); i++){
            if (!visited[list_graph[start_from][i]]){
               visited[list_graph[start_from][i]] = true;
               queue.push_back(list_graph[start_from][i]);
            }
         }
      }
   }

   void BF_FOF(int start_from){
      bool *visited=new bool[this->no_vertex];
      for(int i=0;i<this->no_vertex; i++){
         visited[i]=false;
      }
      int sf=start_from;
      vector<int> *check=new vector<int>[this->no_vertex];

      vector<int> queue;
      visited[start_from] = true;
      queue.push_back(start_from);

      while(!queue.empty()){
        start_from = queue.front();
        queue.erase(queue.begin());

            for (int i = 0; i<list_graph[start_from].size(); i++){
               if(start_from==sf){
                  check[list_graph[start_from][i]].push_back(start_from);
               }else{
                  if (find(check[start_from].begin(), check[start_from].end(), sf) != check[start_from].end()){
                     cout<<list_graph[start_from][i]<<" ";
                  }
               }

               if (!visited[list_graph[start_from][i]]){
                  visited[list_graph[start_from][i]] = true;
                  queue.push_back(list_graph[start_from][i]);
               }
        }
    }
   }
};

int main(){
   int V = 9; // No of vertices
   graph g1(V);
   g1.addToGraph(0, 1);
   g1.addToGraph(0, 2);
   g1.addToGraph(1, 3);
   g1.addToGraph(2, 0);
   g1.addToGraph(3, 2);
   g1.addToGraph(3, 7);
   g1.addToGraph(4, 5);
   g1.addToGraph(4, 6);
   g1.addToGraph(4, 7);
   g1.addToGraph(5, 1);
   g1.addToGraph(6, 3);
   g1.addToGraph(7, 2);
   g1.addToGraph(7, 8);
   g1.addToGraph(8, 4);
   cout<<"BFS Result from 0 : "; g1.BFS(0); cout<<endl;
   cout<<"BFS Result from 1 : "; g1.BFS(1); cout<<endl;
   cout<<"BFS Result from 2 : "; g1.BFS(2); cout<<endl;
   cout<<"BFS Result from 3 : "; g1.BFS(3); cout<<endl;
   cout<<"BFS Result from 4 : "; g1.BFS(4); cout<<endl;
   cout<<"BFS Result from 5 : "; g1.BFS(5); cout<<endl;
   cout<<"BFS Result from 6 : "; g1.BFS(6); cout<<endl;
   cout<<"BFS Result from 7 : "; g1.BFS(7); cout<<endl;
   cout<<"BFS Result from 8 : "; g1.BFS(8); cout<<endl;
   cout<<endl;g1.printGraph();cout<<endl;
   cout<<"Friends of Friends of 0 : [ "; g1.BF_FOF(0); cout<<"]"<<endl;
   cout<<"Friends of Friends of 1 : [ "; g1.BF_FOF(1); cout<<"]"<<endl;
   cout<<"Friends of Friends of 2 : [ "; g1.BF_FOF(2); cout<<"]"<<endl;
   cout<<"Friends of Friends of 3 : [ "; g1.BF_FOF(3); cout<<"]"<<endl;
   cout<<"Friends of Friends of 4 : [ "; g1.BF_FOF(4); cout<<"]"<<endl;
   cout<<"Friends of Friends of 5 : [ "; g1.BF_FOF(5); cout<<"]"<<endl;
   cout<<"Friends of Friends of 6 : [ "; g1.BF_FOF(6); cout<<"]"<<endl;
   cout<<"Friends of Friends of 7 : [ "; g1.BF_FOF(7); cout<<"]"<<endl;
   cout<<"Friends of Friends of 8 : [ "; g1.BF_FOF(8); cout<<"]"<<endl;
   return 0;
}